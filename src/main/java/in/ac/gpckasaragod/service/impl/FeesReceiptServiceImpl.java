/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.service.impl;

import in.ac.gpckasaragod.model.DepartmentUIModel;
import in.ac.gpckasaragod.pta.fees.receipt.system.ui.data.Department;
import in.ac.gpckasaragod.pta.fees.receipt.system.ui.data.FeesReceipt;
import in.ac.gpckasaragod.service.DepartmentService;
import in.ac.gpckasaragod.service.FeesReceiptService;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class FeesReceiptServiceImpl extends ConnectionServiceImpl implements FeesReceiptService {

    @Override
    public String saveFeesReceipt(String name, String fatherName, String admissionNo, Integer amount, Integer deptId) {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "INSERT INTO FEES_RECEIPT_DETAILS (NAME,FATHER_NAME,DMISSION_NO,AMOUNT) VALUES"
                    + "('" +name+ "','" +fatherName+"','"+admissionNo+"','"+amount+"','"+deptId+"')";

            System.err.println("query:" + query);
            int status = statement.executeUpdate(query);
            if (status != 1) {
                return "Save failed";
            } else {
                return "Saved successfully";
            }
        } catch (SQLException ex) {
            Logger.getLogger(FeesReceiptServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            return "Save failed";
    }
    }
     @Override
       public FeesReceipt readFeesReceipt(Integer id){
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        FeesReceipt feesReceipt = null;
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM FEES_RECEIPT_DETAILS WHERE ID="+id;
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                id = resultSet.getInt("ID");
                String name = resultSet.getString("NAME");
                String fatherName = resultSet.getString("FATHER_NAME");
                String admissionNo = resultSet.getString("ADMISSION_NO");
                Integer amount = resultSet.getInt("AMOUNT");
                Integer deptId = resultSet.getInt("DEPT_ID");
                feesReceipt = new FeesReceipt(id, name, fatherName,admissionNo,amount,deptId);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DepartmentServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return feesReceipt;
    }

    @Override
    public List<DepartmentUIModel> getAllFeesReceipts() {
        // throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
       List<DepartmentUIModel> departments = new ArrayList<>();
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM FEES_RECEIPT_DETAILS";
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                Integer id = resultSet.getInt("ID");
                String name = resultSet.getString("NAME");
                String shortName = resultSet.getString("SHORT_NAME");
                String fName = resultSet.getString("NAME");
                String fatherName = resultSet.getString("FATHER_NAME");
                String admissionNo = resultSet.getString("ADMISSION_NO");
                Integer amount = resultSet.getInt("AMOUNT");
                Integer deptId = resultSet.getInt("DEPT_ID");
  DepartmentUIModel departmentUIModel = new DepartmentUIModel(id,name,shortName,fName, fatherName,admissionNo,amount,deptId);
           departments.add(departmentUIModel);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DepartmentServiceImpl.class.getName()).log(Level.SEVERE, null, ex);

        }
        return departments;
    }
 @Override
    public String updateFeesReceipt(Integer id, String name, String fatherName,String admissionNo,Integer amount,Integer deptId) {
        try {
            //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "UPDATE FEES_RECEIPT_DETAILS SET NAME='" + name + "',FATHER_NAME='" + fatherName + "',ADMISSION_NO='"+ admissionNo+"',AMOUNT='"+ amount +"',DEPT_ID='"+ deptId +"'WHERE ID=" + id;
            System.out.print(query);
            int update = statement.executeUpdate(query);
            if (update != 1) {
                return "Update failed";
            } else {
                return "Upate successfully";
            }
        } catch (SQLException ex) {
            return "Update failed";
        }
    }
      @Override
    public String DeleteFeesReceipt(Integer Id) {
        //  throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        try {
            Connection connection = getConnection();
            String query = "DELETE FROM FEES_RECEIPT_DETAILS WHERE ID +?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, Id);
            int delete = statement.executeUpdate();
            if (delete != 1) {
                return "Delete failed";
            } else {
                return "Delete successfully";
            }
        } catch (SQLException ex) {

            return "Delete failed";

        }
    }
    
}