/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.service;



//import in.ac.gpckasaragod.pta.fees.receipt.system.ui.data.Department;
import in.ac.gpckasaragod.pta.fees.receipt.system.ui.data.Department;
import java.util.List;


/**
 *
 * @author student
 */
public interface DepartmentService {
    public String saveDepartment(String name,String shortName);
    public Department readDepartment(Integer id);
   public List<Department>getAllDepartments();
    public String updateDepartment(Integer id,String name,String shortName);
      public String DeleteDepartment(Integer Id);
}
