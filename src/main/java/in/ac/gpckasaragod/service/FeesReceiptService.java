/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.service;

import in.ac.gpckasaragod.model.DepartmentUIModel;
import in.ac.gpckasaragod.pta.fees.receipt.system.ui.data.FeesReceipt;
import java.util.List;

/**
 *
 * @author student
 */
public interface FeesReceiptService {
     public String saveFeesReceipt(String name, String fatherName,String admissionNo,Integer amount,Integer deptId) ;
    public FeesReceipt readFeesReceipt(Integer id);
     public List<DepartmentUIModel> getAllFeesReceipts();
     public String updateFeesReceipt(Integer id, String name, String fatherName,String admissionNo,Integer amount,Integer deptId); 
     public String DeleteFeesReceipt(Integer Id);

    public String saveFeesReceipt(String name, String fatherName, String admissionNo, String amount, Integer id);

    public String updateFeesReceipt(Integer selectedId, String name, String fatherName, String admissionNo, String amount, Integer id);

}
