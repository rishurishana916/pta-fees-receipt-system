/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.model;

/**
 *
 * @author student
 */
public class DepartmentUIModel {
    private Integer id;
    private String departmentName;
     private String shortName;
     private String name;
     private String fatherName;
     private String admissionNo;
     private Integer amount;
     public DepartmentUIModel(String departmentname,String shortName,String name,String fatherName,String admissionNo,Integer amount){
         this.id = id;
         this.departmentName=departmentname;
         this.shortName = shortName;
         this.name = name;
         this.fatherName = fatherName;
         this.admissionNo = admissionNo;
         this.amount= amount;
     }

    public DepartmentUIModel(Integer id, String name, String fatherName, String admissionNo, Integer amount, Integer deptId) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public DepartmentUIModel(Integer id, String name, String shortName, String name0, String fatherName, String admissionNo, Integer amount, Integer deptId) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getAdmissionNo() {
        return admissionNo;
    }

    public void setAdmissionNo(String admissionNo) {
        this.admissionNo = admissionNo;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }
     
}
